<!-- <!-- Опишіть своїми словами що таке Document Object Model (DOM) -->
 Document Object Model(DOM) - це програмний інтерфейс, який дозволяє взаємодіяти з HTML - документами і XML -
  документами як з об'єктами. DOM створює деревоподібну структуру, де кожен тег HTML документа перетворюється в об'єкт,
  що може бути доступний для зміни через JavaScript.За допомогою DOM можна динамічно змінювати структуру і зміст HTML -
  документа, який відображається в
  браузері.Наприклад, можна додавати нові елементи, змінювати стилі, видаляти елементи та змінювати їх атрибути. DOM є
  ключовою складовою багатьох веб - застосунків, таких як динамічні веб - сайти, веб - додатки, фреймворки
  JavaScript і інші.Використовуючи DOM, розробники можуть створювати інтерактивні веб - сторінки, які можуть взаємодіяти
  з користувачем і реагувати на його дії.


<!-- Яка різниця між властивостями HTML-елементів innerHTML та innerText? -->
Властивість innerHTML містить HTML-код всередині елементу, включаючи теги, атрибути та текстовий контент. Це означає,
  що властивість innerHTML поверне всі HTML-елементи та текстовий контент, які знаходяться всередині вказаного елементу.
  Наприклад, якщо ми маємо елемент <p>Hello <strong>World!</strong></p>, то innerHTML поверне рядок Hello
  <strong>World!</strong>. З іншого боку, властивість innerText поверне тільки видимий текст всередині елементу,
  ігноруючи будь-які HTML-теги або
  їх атрибути. Це означає, що якщо ми маємо елемент <p>Hello <strong>World!</strong></p>, то innerText поверне рядок
  Hello World!. Отже, головна різниця полягає в тому, що innerHTML повертає HTML-код всередині елементу, включаючи теги,
  а innerText
  повертає тільки видимий текст.


<!-- Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий? --> -->
Можна звернутися до елемента сторінки за допомогою, використанням методу document.getElementById() або методів
  document.querySelector() / document.querySelectorAll(). Метод document.getElementById() звертається до елемента з
  унікальним ідентифікатором (id) та повертає посилання на цей
  елемент.Методи document.querySelector() та document.querySelectorAll() дозволяють звернутися до елемента за допомогою
  CSS-селекторів. document.querySelector() поверне перший елемент, який відповідає CSS-селектору, а
  document.querySelectorAll() поверне список всіх елементів, які відповідають CSS-селектору.У загальному випадку, якщо
  відомо, що елемент є унікальним на сторінці, то метод document.getElementById() є швидшим і кращим варіантом. Якщо ж
  потрібно звернутися до кількох елементів, то краще використовувати методи document.querySelector() та
  document.querySelectorAll()

